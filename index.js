"use strict";

function factorial(number) {
  if (!isNaN(number)) {
    if (+number === 0) {
      return 1;
    } else {
      let result = number * factorial(number - 1);
      return result;
    }
  }
}

let userNumber = prompt(
  "Введіть число, для якого потрібно рреалізувати функцію підрахунку факторіалу числа:"
);
while (isNaN(userNumber)) {
  alert("Введено не число");
  userNumber = prompt("Введіть число знову:");
}
let n = factorial(parseInt(userNumber));
alert(`Факторіал числа ${userNumber} буде: ${n}`);
